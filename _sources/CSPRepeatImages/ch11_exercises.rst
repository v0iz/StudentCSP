..  Copyright (C)  Brad Miller, David Ranum, Jeff Elkner, Peter Wentworth,
    Allen B. Downey, Chris Meyers, and Dario Mitchell.  Permission is granted
    to copy, distribute and/or modify this document under the terms of the GNU
    Free Documentation License, Version 1.3 or any later version published by
    the Free Software Foundation; with Invariant Sections being Forward,
    Prefaces, and Contributor List, no Front-Cover Texts, and no Back-Cover
    Texts.  A copy of the license is included in the section entitled "GNU Free
    Documentation License".


.. setup for automatic question numbering.

.. qnum::
    :start: 1
    :prefix: 11-8-

Chapter 11 Exercises
---------------------

Below is a selection of images that you can use in the programs in this
section.

.. raw:: html

   <table>
   <tr><td>beach.jpg</td><td>baby.jpg</td><td>vangogh.jpg</td><td>swan.jpg</td></tr>
   <tr><td><img src="../_static/beach.jpg" id="beach.jpg"></td><td><img src="../_static/baby.jpg" id="baby.jpg"></td><td><img src="../_static/vangogh.jpg" id="vangogh.jpg"></td><td><img src="../_static/swan.jpg" id="swan.jpg"></td></tr>
   </table>
   <table>
   <tr><td>puppy.jpg</td><td>kitten.jpg</td><td>girl.jpg</td><td>motorcycle.jpg</td></tr>
   <tr><td><img src="../_static/puppy.jpg" id="puppy.jpg"></td><td><img src="../_static/kitten.jpg" id="kitten.jpg"></td><td><img src="../_static/girl.jpg" id="girl.jpg"></td><td><img src="../_static/motorcycle.jpg" id="motorcycle.jpg"></td></tr>
   </table>
   <table>
   <tr><td>gal1.jpg</td><td>guy1.jpg</td><td>gal2.jpg</td></tr>
   <tr><td><img src="../_static/gal1.jpg" id="gal1.jpg"></td><td><img src="../_static/guy1.jpg" id="guy1.jpg"></td><td><img src="../_static/gal2.jpg" id="gal2.jpg"></td></tr>
   </table>

.. note::

   Remember that it can take a bit of time to process all the pixels in a
   picture!  Check for errors below the code if it is taking a long time, but
   if you don't see any errors just wait.

#.
    .. tabbed:: ch11ex1t

        .. tab:: Question

            Fix 6 syntax errors in the code below so that it correctly sets the
            red in all pixels to 0.

            .. activecode:: ch11ex1q
                :nocodelens:

                # Use the image library
                from PIL import

                # Connect to an image file
                img = Image.open('gal2.jpg'

                # Load the pixels 
                pixels = img.load

                # Loop through the pixels
                for x in range(img.size[0]):
                    for y in range(img.size[1]):
                        r, g, b = pixels[]

                        # Set the red to 0
                        pixels[x, y] =  , g, b

                # Show the result
                img.show

#.
    .. tabbed:: ch11ex2t

        .. tab:: Question

            The code below makes the image have a green-blue tint. Change 1
            thing in order to make it have a red tint instead.

            .. activecode::  ch11ex2q
                :nocodelens:

                # Use the image library
        	from PIL import Image

                # Connect to the image 
        	img = Image.open('puppy.jpg')

                # Load the pixels 
                pixels = img.load()

                # Loop through the pixels
                for x in range(img.size[0]):
                    for y in range(img.size[1]):
                        r, g, b = pixels[x, y]

                        # Set the color
                        pixels[x, y] = 0, g, b

                # Show the result
                img.show()

        .. tab:: Discussion

            .. disqus::
                :shortname: teachercsp
                :identifier: teachercsp_ch11ex2q

#.
    .. tabbed:: ch11ex3t

        .. tab:: Question

           Fix the indention below to correctly set the red to the green, the
           green to the blue, and the blue to the red.

           .. activecode::  ch11ex3q
                :nocodelens:

                # Step 1: Get the Image object from the PIL library
        	from PIL import Image

                # Step 2: Connect to the image 
                img = Image.open('beach.jpg')

                # Step 3: Load the pixels
                pixels = img.load() 

                # Step 4: Loop through the pixels
                for x in range(img.size[0]):
                for y in range(img.size[1]):

                # Step 5: Get the data
                r, g, b = pixels[x, y]

                # Step 6: Modify the color
                pixels[x, y] = g, b, r

                # Step 7: Show the result
                img.show()

#.
    .. tabbed:: ch11ex4t

        .. tab:: Question

            Fix the 5 errors in the code, so that the Red pixels get the value
            of the green, the green get the value of blue, and the blue get the
            value of the red. (The cat should look purple and gray)

            .. activecode::  ch11ex4q
                :nocodelens:

                # Step 1: Load the Image class from the PIL library 
		from PIL import Image 

                # Step 2: Connect to the image 
                img = Image.open'kitten.jpg')

                # Step 3: Load the pixels 
                pixels = imgload()

                # Step 4: Loop through the pixels
                for x in range(img.size[]):
                    for y in range(img.size[1]):

                        # Step 5: Get the data
                        r, g, b = pixels[x, y[

                        # Step 6: Modify the color
                        pixels[x y] = g, b, r
 
                # Step 7: Show the result
                img.show()

#.
    .. tabbed:: ch11ex5t

        .. tab:: Question

           Fill in the missing code on lines 9, 12, and 18 below to set the red
           to half the original value in all pixels in the picture.

           .. activecode::  ch11ex5q
                :nocodelens:

                # Step 1: Load the Image class from the PIL library 
                from PIL import Image 

                # Step 2: Connect to the image 
                img = Image.load('beach.jpg')

                # Step 3: Load the pixels 
                pixels = img.load()

                # Step 4: Loop through the pixels
                for x in ?? 
                    for y in ?? 

                    # Step 5: Get the data
                    ?? = ?? 

                    # Step 6: Modify the color
                    pixels[x, y] = ??, ??, ??


                # Step 7: Show the result
                img.show()

#.
    .. tabbed:: ch11ex6t

        .. tab:: Question

            Complete the code in order to set the blue value to an eighth of
            the green value plus an eighth of the red value.

            .. activecode::  ch11ex6q
                :nocodelens:

                from PIL import Image 

                img = Image.open('swan.jpg')
                pixels = img.load()

                for x in range(img.size[0]):
                    for y in range(img.size[1]):

                        r, g, b = pixels[x, y]
                        new_b = ??
                        pixel[x, y] = r, g, new_b
 
                img.show()

#.
    .. tabbed:: ch11ex7t

        .. tab:: Question

           Fix the indention in the code below so that it correctly increases
           the red in each pixel in the picture by 1.5.

           .. activecode::  ch11ex7q
                :nocodelens:

               from PIL import Image 

                   img = Image.open('beach.jpg')
                       pixels = img.load()

               for x in range(img.size[0]):
               for y in range(img.size[1]):

               r, g, b = pixels[x, y]
               pixels[x, y] = int(1.5 * r), g, b

                   img.show()

#.
    .. tabbed:: ch11ex8t

        .. tab:: Question

            This code is supposed to make the picture completely black;
            however, it is taking forever when it should only take a few
            seconds. Fix the code (without adding anything new) so that it runs
            in a few seconds.

            .. activecode::  ch11ex8q
                :nocodelens:

                from PIL import Image 

                img = Image.open('motorcycle.jpg')
                pixels = img.load()

                for x in range(img.size[0]):
                    for y in range(img.size[1]):
                        pixels[x, y] = 0, 0, 0
                        img.show()

#.
    .. tabbed:: ch11ex9t

        .. tab:: Question

           Fix the code below to correctly set the green and blue values to
           0.75 times their current values.

           .. activecode::  ch11ex9q
                :nocodelens:

                # Step 1: Import the Image class from the PIL library
        	from PIL import Image

                # Step 2: Connect to the image 
        	img = Image.open('beach.jpg')

                # Step 3: Load the pixels 
                pixels = img.load()

                # Step 4: Loop through the pixels
                for x in range(img.size[0]):
                    for y in range(img.size[1]):

                        # Step 5: Get the data
                        r, g, b = pixels[x, y]

                        # Step 6: Modify the data
                        pixels[x, y] = r, int(??), int(??)

                # Step 7: Show the result
                img.show()

#.
    .. tabbed:: ch11ex10t

        .. tab:: Question

            Write code below to set all the pixels to half their original
            values. Use the `//` operator so that the color values are
            integers.

            .. activecode::  ch11ex10q
                :nocodelens:

#.

    .. tabbed:: ch11ex11t

        .. tab:: Question

            The following code redraws the whole image by drawing the top half
            and then drawing the bottom half. Change it to set the red to 0 for
            all pixels in the top and the green to 0 in all the pixels in the
            bottom of the picture.

            .. activecode::  ch11ex11q
                :nocodelens:

                from PIL import Image

                img = Image.open('gal2.jpg')
                pixels = img.load()

                for x in range(img.size[0]):
    	            for y in range(img.size[1] // 2):
    	                r, g, b = pixels[x, y]
                        pixels[x, y] = r, g, b
    	            for y in range(img.size[1] // 2, img.size[1]):
    	                r, g, b = pixels[x, y]
                        pixels[x, y] = r, g, b

                img.show()

#.
    .. tabbed:: ch11ex12t

        .. tab:: Question

            The code below makes the whole image have a blue-green tint.
            Change the code so that it makes an only blue tint in the bottom
            left corner.

            .. activecode::  ch11ex12q
                :nocodelens:

                from PIL import Image 

                img = Image.open('vangogh.jpg')
                pixels = img.load()
                width = img.size[0]
                height = img.size[1]

        	for x in range(width):
        	    for y in range(height):
        	        r, g, b = pixels[x, y]
                        pixels[x, y] = 0, g, b

        	img.show()

#.
    .. tabbed:: ch11ex13t

        .. tab:: Question

            Change the code below to set the red value in the pixels in the
            bottom half of the picture to 0.

           .. activecode::  ch11ex13q
                :nocodelens:

                from PIL import Image 

                img = Image.open('gal2.jpg')
                pixels = img.load()
                width = img.size[0]
                height = img.size[1]

        	for x in range(width):
        	    for y in range(height):
        	        r, g, b = pixels[x, y]
                        pixels[x, y] = r, g, b

        	img.show()

#.
    .. tabbed:: ch11ex14t

        .. tab:: Question

            The code below makes the whole image seem red. Change it, so that
            only every 5 pixels get changed, so that it will look like a red
            grid.

            .. activecode::  ch11ex14q
                :nocodelens:

                from PIL import Image

                img = Image.open('guy1.jpg')
                w = img.size[0]
                h = img.size[1]
                pixels = img.load()

                for x in range(w):
                    for y in range(h):
                        r, g, b = pixels[x, y]
                        pixels[x, y] = r, 0, 0

                img.show()

#.
    .. tabbed:: ch11ex15t

        .. tab:: Question

            Fill in the missing values in the procedure to keep only the green
            value of each pixels in an image. Call the proceedure with the
            baby.jpg image to test your result.

           .. activecode::  ch11ex15q
                :nocodelens:

                from PIL import Image 

                def show_only_green(image_file_name):
                    img = Image.open(??)
                    w = img.??
                    h = img.??
                    pixels = img.load()

                    for x in range(w):
                        for y in range(h):
                            r, g, b = ??
                            pixels[x, y] = ??, ??, ??

                    img.show()

                # call proceedure with the baby.jpg
                ??

#.
    .. tabbed:: ch11ex16t

        .. tab:: Question

            A grayscale picture is when the red, green, and blue value of a
            pixel are all equal to the average of the original pixel value.
            Write the code to turn the left half of an image into gray scale.

            .. activecode::  ch11ex16q
                :nocodelens:

#.
    .. tabbed:: ch11ex17t

        .. tab:: Question

           Define a procedure to negate an image.  See Image_Negate_Quarter
           from Chapter 11 section 7 for how to create a negative of an image.
           Pass the image to the procedure.  Do the import, create the image,
           call the prodecure, and show the result.

           .. activecode::  ch11ex17q
                :nocodelens:

#.
    .. tabbed:: ch11ex18t

        .. tab:: Question

           Write code that takes the top half of an image and replicates it in
           the bottom half.

            .. activecode::  ch11ex18q
                :nocodelens:

#.
    .. tabbed:: ch11ex19t

        .. tab:: Question

           Write a procedure to mirror an image from left to right around a
           vertical line in the middle of the image.  Pass the image to the
           procedure.  Do the import, create the image, call the prodecure, and
           show the result.

           .. activecode::  ch11ex19q
               :nocodelens:

#.
    .. tabbed:: ch11ex20t

        .. tab:: Question

            Write code that flips the image across a horizontal line.

            .. activecode::  ch11ex20q
                :nocodelens:
