..  Copyright (C)  Brad Miller, David Ranum, Jeffrey Elkner, Peter Wentworth,
    Allen B. Downey, Chris Meyers, and Dario Mitchell.  Permission is granted
    to copy, distribute and/or modify this document under the terms of the GNU
    Free Documentation License, Version 1.3 or any later version published by
    the Free Software Foundation; with Invariant Sections being Forward,
    Prefaces, and Contributor List, no Front-Cover Texts, and no Back-Cover
    Texts.  A copy of the license is included in the section entitled "GNU Free
    Documentation License".


.. setup for automatic question numbering.

.. qnum::
    :start: 1
    :prefix: 9-6-

Chapter 9 Exercises
-------------------

#.
    .. tabbed:: ch9ex1t

        .. tab:: Question

            Fix 5 errors in code below to correctly print: "Rubber baby buggy
            bumpers."

            .. activecode:: ch9ex1q
                :nocodelens:

                # Step 1: Initialize accumulator
                new_string = ""

                # Step 2: Get data
                Phrase = "Rubber baby buggy bumpers.

                # Step 3: Loop through the data
                for letter in phrase
                    # Step 4: Accumulate
                    new_string = new_string letter

                # Step 5: Process result
                print(new_string

#.
    .. tabbed:: ch9ex2t

        .. tab:: Question

            The code currently prints the reverse of a string. Change it so
            that it prints the string in the correct order, but every character
            is separated by a space (there should even be a space between a
            space and the next character).

            .. activecode::  ch9ex2q
                :nocodelens:

                # Step 1: Initialize accumulator
                new_string = ""

                # Step 2: Get data
                phrase = "This is a string."

                # Step 3: Loop through the data
                for letter in phrase:
                    # Step 4: Accumulate
                    new_string = letter + new_string

                # Step 5: Process result
                print(new_string)

#.
    .. tabbed:: ch9ex3t

        .. tab:: Question

           Fix the indention on 3 lines below to correctly print the reverse of
           the string.  It should print: "!yadhtriB yppaH."

           .. activecode::  ch9ex3q
                :nocodelens:

                # Step 1: Initialize accumulator
                new_string = ""

                # Step 2: Get data
                    phrase = "Happy Birthday!"

                # Step 3: Loop through the data
                for letter in phrase:
                # Step 4: Accumulate
                new_string = letter + new_string

                # Step 5: Process result
                    print(new_string)

#.
    .. tabbed:: ch9ex4t

        .. tab:: Question

            Fix the errors in the code to correctly print the reverse of the
            string. It should print: "!gnirts a m'I ,kool yeH"

            .. activecode::  ch9ex4q
                :nocodelens:

                # Step 2: Get data
                phrase = "Hey look, I'm a string!"

                # Step 3: Loop through the data
                for letter in phrase:
                    new_string = ""
                    # Step 4: Accumulate
                    new_string = new_string + phrase

                    # Step 5: Process result
                    print(phrase)

#.
    .. tabbed:: ch9ex5t

        .. tab:: Question

           Fix 4 errors in the code below to correctly print the mirror of the
           text in phrase.  It should print: "tset a si sihTThis is a test."

           .. activecode::  ch9ex5q
                :nocodelens:

                # Step 1: Initialize accumulator
                new_string =

                # Step 2: Get data
                phrase = "This is a test"

                # Step 3: Loop through the data
                for l in phrase:
                    # Step 4: Accumulate
                    new_string = letter + new_string  letter

                # Step 5: Process result
                print()

#.
    .. tabbed:: ch9ex6t

        .. tab:: Question

            The code currently prints each letter of the string twice in a row.
            Change it so that it prints the mirror of the string. It should
            print: "!rorrim a ni gnikool ekil s'tIIt's like looking in a
            mirror!"

            .. activecode::  ch9ex6q
                :nocodelens:

                # Step 1: Initialize accumulator
                new_string = ""

                # Step 2: Get data
                phrase = "It's like looking in a mirror!"

                # Step 3: Loop through the data
                for letter in phrase:
                    # Step 4: Accumulate
                    new_string = new_string + letter + letter

                # Step 5: Process result
                print(new_string)

#.
    .. tabbed:: ch9ex7t

        .. tab:: Question

           The code below is supposed to replace all 1's with i's, but it is in
           an infinite loop.  You can reload the page to stop the infinite
           loop.  Add a line to make the code work.  It should print: "This is
           a string."

           .. activecode::  ch9ex7q
                :nocodelens:

                a_str = "Th1s is a str1ng"
                pos = a_str.find("1")
                while pos >= 0:
                    a_str = a_str[0:pos] + "i" + a_str[pos+1:len(str)]
                print(a_str)

#.
    .. tabbed:: ch9ex8t

        .. tab:: Question

            Fix the errors so that the code prints "I'm just a string."

            .. activecode::  ch9ex8q
                :nocodelens:

                # Step 1: Initialize accumulator
                new_string = "  "

                # Step 2: Get data
                phrase = "I'm just a string."

                # Step 3: Loop through the data
                for phrase in letter
                    # Step 4: Accumulate
                    letter = letter + new_string

                # Step 5: Process result
                print(new_string)

#.
    .. tabbed:: ch9ex9t

        .. tab:: Question

           The program below is supposed to encode the text in message, but it
           has 5 errors.  Fix the errors so that it prints:
           "nvvg.nv.zg.nrwmrtsg."

           .. activecode::  ch9ex9q
                :nocodelens:

                message = "meet me at midnight"
                a_str = "abcdefghijklmnopqrstuvwxyz.
                e_str = zyxwvutsrqponmlkjihgfedcba ."
                encoded_message = message

                for letter in message
                    pos = a_str.find(letter)
                    encoded_message = encoded_message + e_str[pos:pos+1]

                print encoded_message)

#.
    .. tabbed:: ch9ex10t

        .. tab:: Question

            The code currently prints "This is a striniThis is a string". Fix
            the error so that it replaces every "1" with "i" and prints "This
            is a string".

            .. activecode::  ch9ex10q
                :nocodelens:

                a_str = "Th1s is a str1ng"
                pos = a_str.find("1")

                while pos >= 0:
                    pos = a_str.find("1")
            	    a_str = a_str[0:pos] + "i" + a_str[pos+1:len(a_str)]

                print(a_str)

#.
    .. tabbed:: ch9ex11t

        .. tab:: Question

           Rewrite the following code to create a function that takes a string
           and returns the reverse of the string.  It should print: "!yadhtriB
           yppaH."

           .. activecode::  ch9ex11q
                :nocodelens:

                # Step 1: Initialize accumulators
                new_string = ""

                # Step 2: Get data
                phrase = "Happy Birthday!"

                # Step 3: Loop through the data
                for letter in phrase:
                    # Step 4: Accumulate
                    new_string = letter + new_string

                # Step 5: Process result
                print(new_string)

#.
    .. tabbed:: ch9ex12t

        .. tab:: Question

            Fix the errors in the code so that it replaces the misspelled word
            "recieved" with the correct spelling "received"

            .. activecode::  ch9ex12q
                :nocodelens:

                a_str = "He recieved candy"
                pos = a_str.find("received")

                while pos >= 0:
                    a_str = a_str[0:pos+len("recieved")] + "received" + a_str[pos:len(str)]
                    pos = a_str.find("recieved")

                print(a_str)

#.
    .. tabbed:: ch9ex13t

        .. tab:: Question

           Rewrite the following code to create a function that takes a string
           and returns the mirror of the string.  It should print: "!ssalC iHHi
           Class!".

           .. activecode::  ch9ex13q
                :nocodelens:

                # Step 1: Initialize accumulator
                new_string = ""

                # Step 2: Get data
                phrase = "This is a test"

                # Step 3: Loop through the data
                for letter in phrase:
                    # Step 4: Accumulate
                    new_string = letter + new_string + letter

                # Step 5: Process result
                print(new_string)

#.
    .. tabbed:: ch9ex14t

        .. tab:: Question

            Complete the code to change all the periods to commas.

            .. activecode::  ch9ex14q
                :nocodelens:

                a_str = "I like to eat. sleep. learn. and code!"
                pos = a_str.

                while pos >= :
                    a_str = a_str[0:pos] +   + a_str[  :len(a_str)]
                    pos =

                print(str)

#.
    .. tabbed:: ch9ex15t

        .. tab:: Question

           Modify the code below to create a function that will that will take
           a message and return an encoded message.  It should print:
           "nvvg.nv.zg.nrwmrtsg."

           .. activecode::  ch9ex15q
                :nocodelens:

                message = "meet me at midnight"
                a_str = "abcdefghijklmnopqrstuvwxyz. "
                e_str = "zyxwvutsrqponmlkjihgfedcba ."
                encoded_message = ""

                for letter in message:
                    pos = a_str.find(letter)
                    encoded_message = encoded_message + e_str[pos:pos+1]

                print(encoded_message)

#.
    .. tabbed:: ch9ex16t

        .. tab:: Question

            Rewrite and fix the errors in the code to be a procedure that takes
            in a string and prints the reverse of the string and the mirror of
            the string. Make sure to call the procedure.

            .. activecode::  ch9ex16q
                :nocodelens:

                # Step 1: Initialize accumulator
                reverse_string = ""
                mirror_string = " "

                # Step 2: Get data
                phrase = "This is the string"

                # Step 3: Loop through the data
                for phrase in phrase:
                    # Step 4: Accumulate
                    reverse_string = reverse_string + letter
                    mirror_string = letter + letter + reverse_string

                # Step 5: Process result
                print(reverse_string)
                print(mirror_string)

#.
    .. tabbed:: ch9ex17t

        .. tab:: Question

           Modify the code below to create a function
           ``decode(encoded_message)`` that takes the encoded message as an
           argument and returns the decoded string.  It should return: "meet me
           at midnight" when called with "nvvg.nv.zg.nrwmrtsg", and you should
           print this returned value.

           .. activecode::  ch9ex17q
                :nocodelens:

                message = ""
                a_str = "abcdefghijklmnopqrstuvwxyz. "
                e_str = "zyxwvutsrqponmlkjihgfedcba ."
                encoded_message = "nvvg.nv.zg.nrwmrtsg"

                for letter in encoded_message:
                    pos = e_str.find(letter)
                    message = message + a_str[pos]

                print(message)

#.
    .. tabbed:: ch9ex18t

        .. tab:: Question

            Finish the code so that it prints the mirror of the string with the
            correct way then the reverse. It should print: "This is a
            mirror!!rorrim a si sihT"

            .. activecode::  ch9ex18q
                :nocodelens:

                # Step 1: Initialize accumulator
                new_string = ""
                a_string = ""

                # Step 2: Get data
                phrase = "This is a mirror!"

#.
    .. tabbed:: ch9ex19t

        .. tab:: Question

           Create another function that encodes a string.  Pass in both the
           string to be encoded *and* the string to use to encode the string as
           well.

           .. activecode::  ch9ex19q
               :nocodelens:

#.
    .. tabbed:: ch9ex20t

        .. tab:: Question

            Here's the code to encode a message. Write code underneath it to
            decode the encoded message and print it.

            .. activecode::  ch9ex20q
                :nocodelens:

                message = "meet me at midnight"
                a_str = "abcdefghijklmnopqrstuvwxyz. "
                e_str = "zyxwvutsrqponmlkjihgfedcba ."
                encoded_message = ""

                for letter in message:
                    pos = a_str.find(letter)
                    encoded_message = encoded_message + e_str[pos]

                print(encoded_message)
