..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".

.. qnum::
    :start: 1
    :prefix: csp-15-3-

If and Else with Images
=======================

What if we want to identify objects in a picture?  For example we might want to
find the swan in the following picture. One way to get started looking for an
object is to find the points where the color changes substantially from one
pixel to the next.  This is called **edge detection** and it is an important
step in image processing.  

.. raw:: html

    <img src="../_static/swan.jpg" id="swan.jpg">

To look for substantial color changes calculate the average color for the
current pixel and the one to the the right of it. You can calculate the average
as the sum of the red, green, and blue values divided by 3 (the number of
values).  Then compare the absolute value of the difference in the averages and
if it is greater than some amount set the current pixel to black, otherwise set
it to white.  This will result in what looks like a simple pencil sketch of the
picture.  Try larger and smaller values than 10 in line 21 to see how they
change the result.

.. activecode:: Edge_Detection
    :tour_1: "Structural Tour"; 2: id3/line2; 5: id3/line5; 11-12: id3/lines11n12; 13: id3/line13; 14: id3/line14; 15: id3/line15; 16: id3/line16; 19-20: id3/lines19n20; 21-22: id3/lines21n22; 25: id3/line25; 28: id3/line28;
    :nocodelens:

    # Import the Image class from PIL library
    from PIL import Image
    
    # Connect to the image file
    img = Image.open('swan.jpg')

    # Load pixels from image 
    pixels = img.load()

    # Loop through all but last column
    for x in range(img.size[0]-1):
        for y in range(img.size[1]):
            r1, g1, b1 = pixels[x, y]
            r2, g2, b2 = pixels[x + 1, y]
            avg1 = (r1 + g1 + b1) // 3
            avg2 = (r2 + g2 + b2) // 3
          
            # Values for the new color
            if abs(avg2 - avg1) > 10:
            	new_pixel = 0, 0, 0
            else:
            	new_pixel = 255, 255, 255
            
            # Change the image
            pixels[x, y] = new_pixel
        
    # Show the changed image  
    img.show()
    
Notice that the code above loops from 0 to the width - 1 as the last value
through the loop (remember that range doesn't include the last value).  This is
necessary since we are comparing the current pixel's average color with the
average color in the pixel to the right.  There is no pixel to the right of the
last pixel in a row so we have to stop after processing the one before the last
one.

Try other ways to detect big changes in color from one pixel to another.  

.. mchoice:: 15_3_1_Edge
   :answer_a: 0
   :answer_b: 2
   :answer_c: 256
   :answer_d: 16,777,216 (= 256 * 256 * 256) 
   :correct: b
   :feedback_a: Black and white are colors.
   :feedback_b: This image will only have black or white pixels in all columns except the rightmost one. The pixel colors in the rightmost column will not be changed. 
   :feedback_c: This would be true if we were only using shades of gray, but we are only using two colors.  
   :feedback_d: This would be true if we were using all color values, but we are only using black and white.
   
   How many different colors will be in our image (except the last column) when
   we are done?
