..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".

    
.. |audiobutton| image:: Figures/start-audio-tour.png
    :height: 20px
    :align: top
    :alt: audio tour button

.. 	qnum::
	:start: 1
	:prefix: csp-9-2-
	
.. highlight:: python 
   :linenothreshold: 4

Reversing Text
==============

Run this next one, and look at how a simple change to the pattern gives a very
different result.    Here we'll combine *before* rather than *afterward*,
changing only Step 4 (how values are accumulated).

.. activecode:: Copy_Reverse
    :tour_1: "Lines of code"; 2-3: strR2/lines2n3; 6: strR2/line6; 9: strR2/line9; 11: strR2/line11; 12: strR2/line12; 15: strR2/line15; 16: strR2/line16; 17: strR2/line17; 18: strR2/line18;

    # Step 1: Initialize accumulators
    new_string_a = ""
    new_string_b = ""

    # Step 2: Get data
    phrase = "Happy Birthday!"

    # Step 3: Loop through the data
    for letter in phrase:
    	# Step 4: Accumulate
      	new_string_a = letter + new_string_a
      	new_string_b = new_string_b + letter

    # Step 5: Process result
    print("Here's the result of using letter + new_string_a:")
    print(new_string_a)
    print("Here's the result of using new_string_b + letter:")
    print(new_string_b)

.. mchoice:: 9_2_1_Copy_Reverse_Q1
   :answer_a: Because we add each new letter at the <i>beginning</i> of <code>new_string_a</code>.
   :answer_b: Because <code>new_string_a</code> is adding the characters from left to right.
   :answer_c: Because we called a reverse function.
   :answer_d: Because the <code>for</code> loop is doing a reversal
   :correct: a
   :feedback_a: Each new letter gets added at the beginning, which creates a reversal.
   :feedback_b: How would that reverse the other string?
   :feedback_c: There is no reverse function in this program.
   :feedback_d: The same <code>for</code> loop is creating both an in-order copy of the string and a reversed order of the string.  The <code>for</code> loop is the same in both cases.

   Why do you think ``new_string_a`` has all the letters, but in the reverse order?

.. tabbed:: 9_2_2_WSt

        .. tab:: Question

           Write the code to make a palindrome with the string "popsicle". Palindromes read the same foward and backwards. Example: appleelppa

           .. activecode::  9_2_2_WSq
                :nocodelens:

        .. tab:: Answer
            
          .. activecode::  9_2_2_WSa
              :nocodelens:
              
              # INITIALIZE ACCUMULATORS
              new_string_a = ""
              new_string_b = ""
              # NAME DATA
              word = "popsicle"
              # LOOP THROUGH THE DATA
              for letter in word:
                # ACCUMULATE RESULT
                  new_string_a = letter + new_string_a
                  new_string_b = new_string_b + letter
              # DISPLAY RESULT
              print("Here's the result of using new_string_b + letter:")
              print(new_string_b + new_string_a)

