..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".

..  shortname:: Chapter: What You Can Do with a Computer
..  description:: Some tidbits of what you can do with a computer

.. setup for automatic question numbering.

.. qnum::
    :start: 1
    :prefix: csp-1-7-


.. |runbutton| image:: Figures/run-button.png
    :height: 20px
    :align: top
    :alt: run button

.. |audiobutton| image:: Figures/start-audio-tour.png
    :height: 20px
    :align: top
    :alt: audio tour button


Standards - Big Ideas
=====================

This book will address the following Big Ideas from Computer Science [#f1]_:

Big Idea 1: Creativity
----------------------

* Create a computational artifact for creative expression.
* Create a computational artifact using computing tools and techniques to
  solve a problem.
* Create a new computational artifact by combining or modifying existing
  artifacts. 
* Use computing tools and techniques for creative expression. 

Big Idea 2: Abstraction
-----------------------

* Develop an abstraction when writing a program or creating other
  computational artifacts.
* Use multiple levels of abstraction to write programs.
 
Big Idea 3: Data and Information
--------------------------------

* Use computers to process information, find patterns, and test hypotheses
  about digitally processed information to gain insight and knowledge.
* Extract information from data to discover and explain connections, patterns,
  or trends. 
* Use large data sets to explore and discover information and knowledge. 
 
Big Idea 4: Algorithms
----------------------

* Develop an algorithm for implementation in a program. 
* Express an algorithm in a language.
* Explain the existence of undecidable problems in computer science.
* Evaluate algorithms analytically and empirically for efficiency,
  correctness, and clarity.
 
Big Idea 5: Programming
-----------------------

* Develop a program for creative expression, to satisfy personal curiosity,
  or to create new knowledge. 
* Develop a correct program to solve problems. 
* Explain how programs implement algorithms.
* Use abstraction to manage complexity in programs.
* Evaluate the correctness of a program.
* Employ appropriate mathematical and logical concepts in programming.
 
This chapter should have given you a sense for what we are going to be doing
with a computer in this book.  Let's get started in the next chapter by talking
about what the computer can do and how you can control it.  

.. rubric:: Footnotes

.. [#f1] These standards were developed as part of the
   `AP Computer Science Principles
   <https://en.wikipedia.org/wiki/AP_Computer_Science_Principles>`_ curriculum.
