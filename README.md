# StudentCSP
This is an eBook to help students learn the programing standards for the
Computer Science Principles course. It has been created by the CS Learning 4 U
group at Georgia Tech.  For more information see [our
website](https://home.cc.gatech.edu/csl/CSLearning4U)

# Authors
This book was written by 
* Mark Guzdial 
* Barbara Ericson
* Briana Morrison

and remixed by
* Jeff Elkner

# Contributors
* Zhu Zhenyi - changes for working with images
* Christine Alvarado - multiple choice questions
* Miranda Parker - design and A/B testing
* Neeti Pathak - Disqus integration, worked solutions, and answer tabs
* Kavish Patel - additional end-of-chapter exercises
* Rhea Chatterjee - Creativity chapter
* Leonie Reif - Global Impact chapter
* Advaith Venkatakrishnan - What's Next chapter
* Dominic Kynkor, Michael Aki, Prabhav Chawla, Yoonwoo Steven Kim - Practice AP exam questions
* Dominic Kynkor - Internet Chapter design and initial content
* Michael Aki - Interactive activities in Global Impact chapter
* Katie Cunningham - REU Commander
* Jeff (Jochen) Rick - Parsons problems rewrite and enhancements
* Marco Sirabella - PIL image syntax support in skulpt and runestone bug fixes
* Shushantika Barua - Usability testing

# Building and trying it
Install dependencies with `pip install -r requirements.txt` and then build with
`runestone build` and then `runestone serve`. The book will be available at
`http://localhost:8000`.

# License

Copyright © Mark Guzdial and Barbara Ericson, [ericson@cc.gatech.edu]
(mailto://ericson@cc.gatech.edu), 2018

Permission is granted to copy, distribute and/or modify this document under the
terms of the GNU Free Documentation License, Version 1.3 or any later version
published by the Free Software Foundation; with no Invariant Sections, no
Front-Cover Texts, and no Back-Cover Texts.  A copy of the license is included
in the file "LICENSE.txt".
